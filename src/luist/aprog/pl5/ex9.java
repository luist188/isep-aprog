package luist.aprog.pl5;

import java.util.Scanner;

public class ex9 {

	public static void main(String args[]) {
		Scanner scanner = new Scanner(System.in);
		
		int binaryNumber = scanner.nextInt();
		
		if (!isBinary(binaryNumber)) {
			System.out.println("O n�mero introduzido n�o � bin�rio.");
		} else {
			int decimalNumber = 0, exponent = 0, number = binaryNumber;
			
			while (number > 0) {
				int lastDigit = number % 10;
				
				decimalNumber += lastDigit * Math.pow(2, exponent);
				number /= 10;
				exponent++;
			}
			
			System.out.println("O n�mero bin�rio " + binaryNumber + " em decimal � " + decimalNumber);
		}
		
		scanner.close();
	}
	
	public static boolean isBinary(int number) {
		boolean isBinary = true;
		
		while (number > 0) {
			int lastDigit = number % 10;
			
			if (lastDigit == 1 || lastDigit == 0) {
				number /= 10;	
			} else {
				isBinary = false;
				break;
			}
		}
		
		return isBinary;
	}
	
}
