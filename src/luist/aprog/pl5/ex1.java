package luist.aprog.pl5;

import java.text.DecimalFormat;
import java.util.Scanner;

public class ex1 {

	public static void main(String args[]) {
		Scanner scanner = new Scanner(System.in);

		System.out.println("Introduza quantos n�meros quer escrever: ");

		int numbers = scanner.nextInt();
		int sumOfEvens = 0, evensCount = 0;

		for (int i = 1; i <= numbers; i++) {
			System.out.println("N�mero: ");

			int number = scanner.nextInt();

			if (number % 2 == 0) {
				sumOfEvens += number;
				evensCount++;
			}
		}

		if (evensCount != 0) {
			float average = (float) sumOfEvens / (float) evensCount;
			float proportion = (float) evensCount / (float) numbers;
			System.out.println("M�dia dos pares: " + new DecimalFormat("#.##").format(average));
			System.out.println("Propor��o de pares para �mpares: " + new DecimalFormat("#.##").format(proportion));
		} else {
			System.out.println("N�o existe...");
		}

		scanner.close();
	}

}
