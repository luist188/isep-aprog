package luist.aprog.pl5.ec;

import java.util.Scanner;

public class ex3 {
	
	public static void main(String args[]) {
		Scanner scanner = new Scanner(System.in);
		
		System.out.print("Digit a number: ");
		
		int number = scanner.nextInt();
		
		System.out.println("The persistence of " + number + " is " + findPersistence(number));
		
		scanner.close();
	}
	
	public static int findPersistence(int number) {
		
		int persistence = 0;
		
		while (number / 10 >= 1) {
			number = multiplyDigits(number);
			persistence++;
		}
		
		return persistence;
	}
	
	public static int multiplyDigits(int number) {
		int product = 1;
		
		while (number > 0) {
			int lastDigit = number % 10;
			
			product *= lastDigit;
			
			number /= 10;
		}
		
		return product;
	}
}
