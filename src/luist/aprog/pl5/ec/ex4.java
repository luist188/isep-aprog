package luist.aprog.pl5.ec;

import java.util.Scanner;

public class ex4 {

	public static void main(String args[]) {
		Scanner scanner = new Scanner(System.in);

		System.out.print("How many terms from the Fibonacci sequence do you want to be shown? ");

		int numbers = scanner.nextInt();

		printFibonacciSequence(numbers);

		scanner.close();
	}

	public static void printFibonacciSequence(int terms) {
		int sequence[] = new int[terms];

		sequence[0] = 1;
		sequence[1] = 1;

		for (int i = 2; i < terms; i++) {
			sequence[i] = sequence[i - 1] + sequence[i - 2];
		}

		for (int i = 0; i < terms; i++) {
			System.out.println("Term number" + (i + 1) + " : " + sequence[i]);
		}
	}

}
