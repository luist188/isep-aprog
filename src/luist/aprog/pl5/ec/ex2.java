package luist.aprog.pl5.ec;

import java.util.Scanner;

public class ex2 {

	public static void main(String args[]) {
		Scanner scanner = new Scanner(System.in);

		System.out.println("How many numbers do you want to convert?");

		try {
			int number = scanner.nextInt();

			for (int i = 1; i <= number; i++) {
				System.out.print("Digit a binary number: ");
				
				int bNumber = scanner.nextInt();
				
				if (isBinary(bNumber)) {
					System.out.println(bNumber + " in decimal is " + fromBinaryToDecimal(bNumber));
				} else {
					System.out.println("That's not a binary number! Try again...");
					i--;
				}
			}
		} catch (Exception e) {
			System.out.println("That's not a valid number!");
		}

		scanner.close();
	}

	public static int fromBinaryToDecimal(int number) {
		int decimalNumber = 0, exponent = 0;
		
		while (number > 0) {
			int lastDigit = number % 10;
			
			decimalNumber += lastDigit * Math.pow(2, exponent);
			number /= 10;
			exponent++;
		}
		
		return decimalNumber;
	}
	
	public static boolean isBinary(int number) {
		boolean isBinary = true;

		while (number > 0) {
			int lastDigit = number % 10;

			if (lastDigit == 1 || lastDigit == 0) {
				number /= 10;
			} else {
				isBinary = false;
				break;
			}
		}

		return isBinary;
	}
}
