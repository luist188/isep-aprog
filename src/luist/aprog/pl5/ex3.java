package luist.aprog.pl5;

import java.text.DecimalFormat;

import javax.swing.JOptionPane;

public class ex3 {

	public static void main(String args[]) {
		StringBuilder message = new StringBuilder().append("Nomes:\n");

		int peopleCount = 0, ofAgeCount = 0;

		boolean end = false;

		while (!end) {
			String name = JOptionPane.showInputDialog("Introduza um nome");

			if (name.equalsIgnoreCase("zzz")) {
				end = true;
				break;
			} else {
				peopleCount++;

				message.append(name + "\n");

				int age = Integer.parseInt(JOptionPane.showInputDialog("Introduza a idade dessa pessoa"));
				if (age >= 18)
					ofAgeCount++;
			}
		}

		float percentage = ofAgeCount * 100F / peopleCount;
		message.append("\nPercentagem de pessoas com idade maior ou igual a 18 anos: "
				+ new DecimalFormat("#.##").format(percentage) + "%");

		JOptionPane.showMessageDialog(null, message);

	}

}
