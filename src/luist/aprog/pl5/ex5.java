package luist.aprog.pl5;

import java.util.Scanner;

public class ex5 {

	public static void main(String args[]) {
		Scanner scanner = new Scanner(System.in);

		int res = 0, aux = 1;
		int num = scanner.nextInt();

		while (num != 0) {
			int d = num % 10;
			if (d % 2 == 1) {
				res += d * aux;
				aux *= 10;
			}
			num /= 10;
		}

		System.out.println("O resultado �: " + res);
		
		scanner.close();
	}

}
