package luist.aprog.pl5;

import java.util.Scanner;

public class ex6 {

	public static void main(String args[]) {
		Scanner scanner = new Scanner(System.in);

		int number = scanner.nextInt();

		int evenCount[] = new int[2];

		while (number != 0) {
			if (number % 2 == 0) {
				evenCount[0]++;
			}

			number = scanner.nextInt();
		}

		System.out.println("\nA primeira sequ�ncia terminou\n");

		while (number != -1) {
			if (number % 2 == 0) {
				evenCount[1]++;
			}

			number = scanner.nextInt();
		}

		System.out.println("\nA segunda sequ�ncia terminou\n");

		if (evenCount[0] > evenCount[1]) {
			System.out.println("A sequ�ncia com mais n�meros de pares � a primeira");
		} else if (evenCount[0] < evenCount[1]) {
			System.out.println("A sequ�ncia com mais n�meros de pares � a segunda");
		} else {
			System.out.println("As duas sequ�ncias t�m o mesmo n�mero de pares");
		}

		scanner.close();
	}

}
