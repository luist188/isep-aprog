package luist.aprog.pl5;

import java.util.Scanner;

public class ex8 {

	public static void main(String args[]) {
		Scanner scanner = new Scanner(System.in);
		
		int num = scanner.nextInt();
		
		boolean isBinary = true;
		
		while (num > 0) {
			int lastDigit = num % 10;
			
			if (lastDigit == 1 || lastDigit == 0) {
				num /= 10;	
			} else {
				isBinary = false;
				break;
			}
		}
		
		System.out.println(isBinary == true ? "O n�mero introduzido � bin�rio" : "O n�mero introduzido n�o � bin�rio");
		
		scanner.close();
	}
	
}
