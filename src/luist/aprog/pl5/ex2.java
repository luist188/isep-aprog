package luist.aprog.pl5;

import javax.swing.JOptionPane;

public class ex2 {

	public static void main(String args[]) {
		int days = Integer.parseInt(JOptionPane.showInputDialog("Introduza o n�mero de dias"));

		int temperature[] = new int[days];

		StringBuilder message = new StringBuilder();

		for (int i = 0; i < days; i++) {
			temperature[i] = Integer.parseInt(JOptionPane.showInputDialog("Introduza a temperatura do dia " + (i + 1)));
			message.append("Dia " + (i + 1) + ": " + getTemperatureType(temperature[i]) + (i == days ? null : "\n"));
		}
		
		JOptionPane.showMessageDialog(null, message.toString(), "Temperaturas", 1);

	}

	/**
	 * Returns the temperature type of a given temperature, classifying with default values.
	 * 
	 * @param temperature   a given temperature by the user
	 * @return 				the temperature type of a specified temperature
	 */
	public static String getTemperatureType(int temperature) {
		String temperatureType = null;
		
		if (temperature >= -30 && temperature < 9) {
			temperatureType = "Muito Frio (" + temperature + "�C)";
		} else if (temperature < 15) {
			temperatureType = "Frio (" + temperature + "�C)";
		} else if (temperature < 20) {
			temperatureType = "Ameno (" + temperature + "�C)";
		} else if (temperature < 30) {
			temperatureType = "Quente (" + temperature + "�C)";
		} else if (temperature < 50) {
			temperatureType = "Muito Quente (" + temperature + "�C)";
		} else {
			temperatureType = "Temperatura Extrema (" + temperature + "�C)";
		}
		
		return temperatureType;
	}

}
