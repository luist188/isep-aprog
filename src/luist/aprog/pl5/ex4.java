package luist.aprog.pl5;

import java.util.Scanner;

public class ex4 {

	public static void main(String args[]) {
		Scanner scanner = new Scanner(System.in);

		int numbers = scanner.nextInt();
		
		for (int i = 1; i <= numbers; i++) {
			if (isPerfect(i)) {
				System.out.println("O n�mero " + i + " � perfeito");
			}
		}

		scanner.close();
	}

	/**
	 * Method to check if a specified number is perfect. A perfect number is a
	 * number whose sum of all its divisors is equal to him.
	 * 
	 * @param number a specified number
	 * @return returns if the number is perfect or not
	 */
	public static boolean isPerfect(int number) {
		if (number % 2 != 0)
			return false;

		int sumOfFactors = 0;

		for (int i = 1; i <= number / 2; i++) {
			if (number % i == 0) {
				sumOfFactors += i;
			}
		}
		
		if (sumOfFactors == number)
			return true;
		else
			return false;
	}
}
