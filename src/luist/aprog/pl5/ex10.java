package luist.aprog.pl5;

import java.util.Scanner;

public class ex10 {

	public static void main(String arsgs[]) {
		Scanner scanner = new Scanner(System.in);

		StringBuilder binaryNumber = new StringBuilder();

		int decimalNumber = scanner.nextInt();

		int number = decimalNumber;
		int rest;
		while (number >= 1) {
			rest = number % 2;
			
			if (rest == 0) {
				binaryNumber.append("0");
			} else {
				binaryNumber.append("1");
			}
			
			number /= 2;
		}

		System.out.println(binaryNumber.reverse().toString());

		scanner.close();
	}

}
