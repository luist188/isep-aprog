package luist.aprog.pl6.util;

public class CombinationalCalculus {

	/**
	 * This method calculates the factorial of a number
	 * 
	 * @param number an input number, must be positive or 0
	 * @return returns the value of the factorial of the number
	 */
	private static long factorial(int number) {
		long result = 1;
		for (int i = 1; i < number; i++) {
			result *= i;
		}
		return result;
	}

	/**
	 * This method calculates the permutation of a number
	 * 
	 * @param number an input number, must be positive or 0
	 * @return returns the value of the permutation of the number
	 */
	public static long permutation(int number) {
		return factorial(number);
	}

	/**
	 * This method calculates the permutation of a number
	 * 
	 * @param n an input number, must be positive or 0
	 * @param p an input number, must be positive or 0
	 * @return returns the value of the permutation of the number
	 */
	public static long permutation(int n, int p) {
		if (n < p)
			return -1;
		return factorial(n) / factorial(n - p);
	}

	/**
	 * This method calculates the combination Cn,p
	 * 
	 * @param n an input number, must be positive or 0
	 * @param p an input number, must be positive or 0
	 * @return returns the value of the combination Cn,p
	 */
	public static long combination(int n, int p) {
		if (n < p)
			return -1;
		return (long) factorial(n) / (factorial(p) * factorial(n - p));
	}

}
