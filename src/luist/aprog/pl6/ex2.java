package luist.aprog.pl6;

import java.util.Scanner;

public class ex2 {

	public static void main(String args[]) {
		Scanner scanner = new Scanner(System.in);

		System.out.println("Enter the number of students of your class: ");

		int students = scanner.nextInt();

		System.out.println("Enter how many subjects do you want to register: ");

		int subjects = scanner.nextInt();

		String subjectNames[] = new String[subjects];

		int approvedStudents[] = new int[subjects];

		for (int i = 0; i < subjects; i++) {
			System.out.println("Enter the name for the " + (i + 1) + "� subject: ");

			// debug line, for some weird reason it's needed
			scanner.nextLine();
			
			subjectNames[i] = scanner.nextLine();

			System.out.println("Enter the approved students for the " + (i + 1) + "� subject: ");

			approvedStudents[i] = scanner.nextInt();
		}

		for (int i = 0; i < subjects; i++) {
			printRatings(subjectNames[i], students, approvedStudents[i]);
		}

		scanner.close();
	}

	public static void printRatings(String subjectName, int students, int approvedStudents) {
		int percentage = (int) (approvedStudents * 10F / students + 0.5F);

		System.out.println("Subject: " + subjectName);

		String positiveGrades = "- Positive Grades: ", negativeGrades = "- Negative Grades: ";

		for (int i = 0; i < 10; i++) {
			if (i < percentage) {
				positiveGrades += "*";
			} else {
				negativeGrades += "*";
			}
		}

		System.out.println(positiveGrades);
		System.out.println(negativeGrades);
	}

}
