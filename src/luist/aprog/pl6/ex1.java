package luist.aprog.pl6;

import java.util.Scanner;

public class ex1 {

	public static void main(String args[]) {
		Scanner scanner = new Scanner(System.in);

		System.out.print("Type a word: ");
		
		String word = scanner.nextLine();
		
		int wordsTyped = 1;

		while (!isPalindrome(word)) {
			wordsTyped++;

			System.out.print("Type a word: ");
			
			word = scanner.nextLine();
		}
		
		System.out.println(word + " is a palindrome and it took you " + wordsTyped + " words to find a palindrome.");

		scanner.close();
	}

	public static boolean isPalindrome(String word) {
		word = word.toLowerCase();

		boolean isPalindrome = true;

		for (int i = 0; i < word.length() / 2; i++) {
			if (word.charAt(i) != word.charAt(word.length() - 1 - i)) {
				isPalindrome = false;
				break;
			}
		}

		return isPalindrome;
	}
}
