package luist.aprog.pl6;

import java.util.Scanner;

public class ex3 {

	public static void main(String args[]) {

		Scanner scanner = new Scanner(System.in);

		System.out.println("Enter the a, b and c values successively:");

		int a = scanner.nextInt();
		int b = scanner.nextInt();
		int c = scanner.nextInt();

		if (isValid(a, b, c)) {
			System.out.println("ab = " + calculateDegree(a, b, c, Degree.ab));
			System.out.println("ac = " + calculateDegree(a, b, c, Degree.ac));
			System.out.println("bc = " + calculateDegree(a, b, c, Degree.bc));
		} else {
			System.out.println("That't not a valid triangle.");
		}

		scanner.close();
	}

	public static int calculateDegree(int a, int b, int c, Degree degree) {
		int value;
		switch (degree) {
		case ab:
			value = (int) (Math.toDegrees(Math.acos((a * a + b * b - c * c) / (2D * a * b))) + 0.5D);
			break;
		case ac:
			value = (int) (Math.toDegrees(Math.acos((a * a + c * c - b * b) / (2D * a * c))) + 0.5D);
			break;
		case bc:
			value = (int) (Math.toDegrees(Math.acos((b * b + c * c - a * a) / (2D * b * c))) + 0.5D);
			break;
		default:
			value = 0;
		}

		return value;
	}

	public static boolean isValid(int a, int b, int c) {
		if (a + b > c && a + c > b && b + c > a) {
			return true;
		} else {
			return false;
		}
	}

	public static enum Degree {
		ab, ac, bc;
	}

}
